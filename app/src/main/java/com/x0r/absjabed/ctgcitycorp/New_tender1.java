package com.x0r.absjabed.ctgcitycorp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class New_tender1 extends AppCompatActivity {

    Button nwtnder1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_tender1);

        nwtnder1 = (Button)findViewById(R.id.newtender11);

        nwtnder1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(New_tender1.this, New_Tender2.class);
                startActivity(myIntent);
            }
        });

    }
}
