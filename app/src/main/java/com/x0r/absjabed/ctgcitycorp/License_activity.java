package com.x0r.absjabed.ctgcitycorp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class License_activity extends AppCompatActivity {

    Button nwlicense, relicense;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_license);

        nwlicense = (Button) findViewById(R.id.lnewtread);
        relicense = (Button) findViewById(R.id.lrenewtread);


        nwlicense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(License_activity.this, NewTreadLicense.class);
                startActivity(myIntent);
            }
        });

        relicense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(License_activity.this, Re_new_licence.class);
                startActivity(myIntent);
            }
        });
    }
}
