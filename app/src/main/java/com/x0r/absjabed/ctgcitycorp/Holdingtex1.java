package com.x0r.absjabed.ctgcitycorp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Holdingtex1 extends AppCompatActivity {
Button holnxt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_holdingtex1);

        holnxt = (Button)findViewById(R.id.hol1next);

        holnxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(Holdingtex1.this, Holdingtex2.class);
                startActivity(myIntent);
            }
        });

    }
}
