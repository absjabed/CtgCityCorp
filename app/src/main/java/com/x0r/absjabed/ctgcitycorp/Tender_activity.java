package com.x0r.absjabed.ctgcitycorp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Tender_activity extends AppCompatActivity {

    Button newtender;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tender);

        newtender = (Button)findViewById(R.id.nwtndr);

        newtender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(Tender_activity.this, New_tender1.class);
                startActivity(myIntent);
            }
        });


    }
}
