package com.x0r.absjabed.ctgcitycorp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class ContactInfo_activity extends AppCompatActivity {

    Button meyorinfo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_info);

        meyorinfo = (Button)findViewById(R.id.cimeyorjoga);

        meyorinfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(ContactInfo_activity.this, JogaMeyor.class);
                startActivity(myIntent);
            }
        });


    }
}
