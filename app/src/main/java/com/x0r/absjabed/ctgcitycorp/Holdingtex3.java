package com.x0r.absjabed.ctgcitycorp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Holdingtex3 extends AppCompatActivity {

    Button hol3nxt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_holdingtex3);

        hol3nxt = (Button)findViewById(R.id.hol3next);

        hol3nxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(Holdingtex3.this, Holdingtex_recipt.class);
                startActivity(myIntent);
            }
        });


    }
}
