package com.x0r.absjabed.ctgcitycorp;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;
import com.andexert.library.RippleView;

import java.util.Random;

public class MainActivity extends ActionBarActivity
        implements NavigationView.OnNavigationItemSelectedListener{

    ImageButton but1,but2,but3,but4,but5,but6,but7,but8,but9,but10;
    Animation animFliph, animFlipu;

    //Thread Class




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        but1 = (ImageButton) findViewById(R.id.meducation);
        but2 = (ImageButton) findViewById(R.id.mdepartment);
        but3 = (ImageButton) findViewById(R.id.mhealth);
        but4 = (ImageButton) findViewById(R.id.msonod);
        but5 = (ImageButton) findViewById(R.id.movijog);
        but6 = (ImageButton) findViewById(R.id.mjogajog);
        but7 = (ImageButton) findViewById(R.id.mkor);
        but8 = (ImageButton) findViewById(R.id.mtender);
        but9 = (ImageButton) findViewById(R.id.memergency);
        but10 = (ImageButton) findViewById(R.id.mentertainment);

        final Animation animRotate = AnimationUtils.loadAnimation(this, R.anim.anim_rotate);
         animFliph = AnimationUtils.loadAnimation(this,R.anim.flip_h);
         animFlipu = AnimationUtils.loadAnimation(this,R.anim.flip_u);
      //  but5.setAnimation(animFliph);
      //but6.setAnimation(animFlipu);

        //..............................On Click Listeners starts ...........................//
        but1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               //changeFlip(1);
                final RippleView rippleView = (RippleView) findViewById(R.id.rect);
                rippleView.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {

                    @Override
                    public void onComplete(RippleView rippleView) {
                        Intent myIntent = new Intent(MainActivity.this, Education_activity.class);
                        startActivity(myIntent);
                    }

                });
            }
        });

        but2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //changeFlip(2);
                final RippleView rippleView = (RippleView) findViewById(R.id.rect2);
                rippleView.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {

                    @Override
                    public void onComplete(RippleView rippleView) {
                        Intent myIntent = new Intent(MainActivity.this, Department_activity.class);
                        startActivity(myIntent);
                    }

                });
            }
        });

        but3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //changeFlip(3);
                final RippleView rippleView = (RippleView) findViewById(R.id.rect3);
                rippleView.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {

                    @Override
                    public void onComplete(RippleView rippleView) {
                        Intent myIntent = new Intent(MainActivity.this, Health_activity.class);
                        startActivity(myIntent);
                    }

                });
            }
        });

        but4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //changeFlip(4);
                final RippleView rippleView = (RippleView) findViewById(R.id.rect4);
                rippleView.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {

                    @Override
                    public void onComplete(RippleView rippleView) {
                        Intent myIntent = new Intent(MainActivity.this, License_activity.class);
                        startActivity(myIntent);
                    }

                });
            }
        });

        but5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // changeFlip(5);
                final RippleView rippleView = (RippleView) findViewById(R.id.rect5);
                rippleView.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {

                    @Override
                    public void onComplete(RippleView rippleView) {
                        Intent myIntent = new Intent(MainActivity.this, ComplainBox_activity.class);
                        startActivity(myIntent);
                    }

                });
            }
        });

        but6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // changeFlip(6);
                final RippleView rippleView = (RippleView) findViewById(R.id.rect6);
                rippleView.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {

                    @Override
                    public void onComplete(RippleView rippleView) {
                        Intent myIntent = new Intent(MainActivity.this, ContactInfo_activity.class);
                        startActivity(myIntent);
                    }

                });

            }
        });

        but7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // changeFlip(7);
                final RippleView rippleView = (RippleView) findViewById(R.id.rect7);
                rippleView.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {

                    @Override
                    public void onComplete(RippleView rippleView) {
                        Intent myIntent = new Intent(MainActivity.this, Tax_activity.class);
                        startActivity(myIntent);
                    }

                });
                //v.startAnimation(animRotate);
            }
        });
        but8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // changeFlip(8);
                final RippleView rippleView = (RippleView) findViewById(R.id.rect8);
                rippleView.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {

                    @Override
                    public void onComplete(RippleView rippleView) {
                        Intent myIntent = new Intent(MainActivity.this,
                                Tender_activity.class);
                        startActivity(myIntent);
                    }

                });

            }
        });
        but9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeFlip(9);
                final RippleView rippleView = (RippleView) findViewById(R.id.rect9);
                rippleView.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {

                    @Override
                    public void onComplete(RippleView rippleView) {
                        Intent myIntent = new Intent(MainActivity.this, Certificate_activity.class);
                        startActivity(myIntent);
                    }

                });
            }
        });
        but10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeFlip(10);
                final RippleView rippleView = (RippleView) findViewById(R.id.rect10);
                rippleView.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {

                    @Override
                    public void onComplete(RippleView rippleView) {
                        Intent myIntent = new Intent(MainActivity.this, UsefulTools_activity.class);
                        startActivity(myIntent);
                    }

                });
            }
        });

        //..............................On Click Listeners ends ...........................//


        //..............................Ripple Effects ...........................//

        final RippleView rippleView = (RippleView) findViewById(R.id.rect);
        rippleView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  Toast.makeText(MainActivity.this, "Tap ripple", Toast.LENGTH_LONG).show();
                //TODO: onRippleViewClick
            }
        });

        rippleView.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {

            @Override
            public void onComplete(RippleView rippleView) {

               // Toast.makeText(MainActivity.this, "1 tabed ripple complete", Toast.LENGTH_SHORT).show();
            }

        });

        //..............................Ripple Effect ends ...........................//


       /* FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

      //  new TileUpdate().execute("www.google.co.uk"); // Pass in whatever you need a url is just an example we don't use it in this tutorial
       // startThread();
    }




    private void completeSplash(){
        startApp();
        finish(); // Don't forget to finish this Splash Activity so the user can't return to it!
    }

    private void startApp() {

    }


    // Methods goes here..........
    // Thread Method

    void startThread()
    {
        Thread th=new Thread(){
            @Override
            public void run(){
                //
                try
                {
                    for (;;)
                    {
                        Random r1 = new Random();
                       final int i2 = r1.nextInt(11 - 01) + 01;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {

                                    changeFlip(i2);
                                }
                                catch(Exception e)
                                {
                                    e.printStackTrace();
                                }
                            }
                        });

                        Thread.sleep(300);
                        // set the scan for 2 seconds
                    }
                }
                catch (InterruptedException e) {
                }

            }// run
        };

        th.start();
    }

    //-------

    // change Flip method

    void changeFlip(int x){
        Random r = new Random();
        int	ix = r.nextInt(3 - 01) + 01;

        switch(x){

            case 1:
                if(ix==1){
                    but1.setAnimation(animFliph);
                }else{
                    but1.setAnimation(animFlipu);
                }
                break;

            case 2:
                if(ix==1){
                    but2.setAnimation(animFlipu);
                }else{
                    but2.setAnimation(animFliph);
                }
                break;

            case 3:
                if(ix==1){
                    but3.setAnimation(animFliph);
                }else{
                    but3.setAnimation(animFlipu);
                }
                break;

            case 4:
                if(ix==1){
                    but4.setAnimation(animFlipu);
                }else{
                    but4.setAnimation(animFliph);
                }
                break;

            case 5:
                if(ix==1){
                    but5.setAnimation(animFliph);
                }else{
                    but5.setAnimation(animFlipu);
                }
                break;

            case 6:
                if(ix==1){
                    but6.setAnimation(animFlipu);
                }else{
                    but6.setAnimation(animFliph);
                }
                break;

            case 7:
                if(ix==1){
                    but7.setAnimation(animFliph);
                }else{
                    but7.setAnimation(animFlipu);
                }
                break;

            case 8:
                if(ix==1){
                    but8.setAnimation(animFlipu);
                }else{
                    but8.setAnimation(animFliph);
                }
                break;

            case 9:
                if(ix==1){
                    but9.setAnimation(animFliph);
                }else{
                    but9.setAnimation(animFlipu);
                }
                break;

            case 10:
                if(ix==1){
                    but10.setAnimation(animFlipu);
                }else{
                    but10.setAnimation(animFliph);
                }
                break;

        }

    }


    //End of change flip

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.edu) {

            Intent myIntent = new Intent(MainActivity.this, Education_activity.class);
            startActivity(myIntent);
            // Handle the camera action
        } else if (id == R.id.dep) {
            Intent myIntent = new Intent(MainActivity.this, Department_activity.class);
            startActivity(myIntent);

        } else if (id == R.id.hel) {
            Intent myIntent = new Intent(MainActivity.this, Health_activity.class);
            startActivity(myIntent);

        } else if (id == R.id.ovi) {
            Intent myIntent = new Intent(MainActivity.this, ComplainBox_activity.class);
            startActivity(myIntent);

        } else if (id == R.id.jog) {
            Intent myIntent = new Intent(MainActivity.this, ContactInfo_activity.class);
            startActivity(myIntent);

        } else if (id == R.id.tax) {
            Intent myIntent = new Intent(MainActivity.this, Tax_activity.class);
            startActivity(myIntent);

        } else if (id == R.id.ten) {
            Intent myIntent = new Intent(MainActivity.this, Tender_activity.class);
            startActivity(myIntent);

        }else if (id == R.id.lic) {
            Intent myIntent = new Intent(MainActivity.this, License_activity.class);
            startActivity(myIntent);

        }else if (id == R.id.tool) {
            Intent myIntent = new Intent(MainActivity.this, UsefulTools_activity.class);
            startActivity(myIntent);

        }else if (id == R.id.cccproj) {
            Intent myIntent = new Intent(MainActivity.this, CCC_projects.class);
            startActivity(myIntent);

        }else if (id == R.id.nav_share) {
            Intent myIntent = new Intent(MainActivity.this, CCC_connects.class);
            startActivity(myIntent);

        }else if (id == R.id.meyor) {
            Intent myIntent = new Intent(MainActivity.this, Our_mayor.class);
            startActivity(myIntent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
