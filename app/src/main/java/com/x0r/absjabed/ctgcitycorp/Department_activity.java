package com.x0r.absjabed.ctgcitycorp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.view.View;
import android.widget.Button;

public class Department_activity extends AppCompatActivity {

    Button engineering;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_department);

        engineering = (Button) findViewById(R.id.dengineering);

        engineering.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(Department_activity.this, Engineering_dept.class);
                startActivity(myIntent);
            }
        });
    }
}
