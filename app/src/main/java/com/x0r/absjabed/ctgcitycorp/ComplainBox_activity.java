package com.x0r.absjabed.ctgcitycorp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class ComplainBox_activity extends AppCompatActivity {

    Button submit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complain_box);


        submit = (Button)findViewById(R.id.cmsubmitovi);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(ComplainBox_activity.this, MainActivity.class);
                Toast.makeText(ComplainBox_activity.this, "Complain sent!", Toast.LENGTH_SHORT).show();
                startActivity(myIntent);
            }
        });
    }
}
