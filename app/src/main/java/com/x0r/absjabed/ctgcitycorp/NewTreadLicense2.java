package com.x0r.absjabed.ctgcitycorp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class NewTreadLicense2 extends AppCompatActivity {

    Button submit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_tread_license2);

        submit = (Button)findViewById(R.id.tl2submit);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(NewTreadLicense2.this, MainActivity.class);
                Toast.makeText(NewTreadLicense2.this, "Registration request sent!", Toast.LENGTH_SHORT).show();
                startActivity(myIntent);
            }
        });
    }
}
