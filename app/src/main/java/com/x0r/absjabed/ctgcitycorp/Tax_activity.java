package com.x0r.absjabed.ctgcitycorp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Tax_activity extends AppCompatActivity {
Button holdingtax,punokor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tax);

        holdingtax = (Button)findViewById(R.id.holdingtax);
        punokor = (Button)findViewById(R.id.punokor);

        holdingtax.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(Tax_activity.this, Holdingtex1.class);
                startActivity(myIntent);
            }
        });

        punokor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(Tax_activity.this, Puno_kor1.class);
                startActivity(myIntent);
            }
        });


    }
}
