package com.x0r.absjabed.ctgcitycorp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Puno_kor1 extends AppCompatActivity {

    Button punokor1bt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_puno_kor1);


        punokor1bt = (Button)findViewById(R.id.punokor1next);

        punokor1bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(Puno_kor1.this, Puno_kor2.class);
                startActivity(myIntent);
            }
        });
    }
}
