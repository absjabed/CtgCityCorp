package com.x0r.absjabed.ctgcitycorp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Holdingtex2 extends AppCompatActivity {
    Button hol2nxt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_holdingtex2);

        hol2nxt = (Button)findViewById(R.id.hol2next);

        hol2nxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(Holdingtex2.this, Holdingtex3.class);
                startActivity(myIntent);
            }
        });


    }
}
