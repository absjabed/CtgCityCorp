package com.x0r.absjabed.ctgcitycorp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class New_Tender2 extends AppCompatActivity {

    Button tenderapply;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new__tender2);

        tenderapply = (Button)findViewById(R.id.applytender);


        tenderapply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(New_Tender2.this, New_tender3.class);
                startActivity(myIntent);
            }
        });
    }
}
