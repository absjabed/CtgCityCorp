package com.x0r.absjabed.ctgcitycorp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Puno_kor2 extends AppCompatActivity {
    Button punosubmit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_puno_kor2);

        punosubmit = (Button)findViewById(R.id.puno2submit);

        punosubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(Puno_kor2.this, MainActivity.class);
                startActivity(myIntent);
            }
        });
    }
}
