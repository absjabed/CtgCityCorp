package com.x0r.absjabed.ctgcitycorp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class New_tender3 extends AppCompatActivity {

    Button tensub;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_tender3);

        tensub = (Button)findViewById(R.id.tensubmit);

        tensub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(New_tender3.this, New_tender_recipet.class);
                startActivity(myIntent);
            }
        });

    }
}
